// // import React, { useEffect, useRef } from 'react';
// // import Hls from 'hls.js';
// // import axios from 'axios';
// // // import crypto from 'crypto';
// // // import { createDecipheriv } from 'crypto-browserify';


// // const VideoPlayerDash = () => {
// //   const videoRef = useRef(null);
// //   let hls;

// //   const handlePlay = () => {
// //     if (videoRef.current && videoRef.current.paused) {
// //       videoRef.current.play();
// //     }
// //   };

// //   useEffect(() => {
// //     if (videoRef.current) {
// //       const videoElement = videoRef.current;
// //       let token;
// //       (async () => {
// //         if (Hls.isSupported()) {
// //           hls = new Hls();
// //           const response = await axios.get('http://localhost:3100/videos/playlist');
// //           const encryptedM3U8 = response.data;

// //           // Decrypt the encrypted M3U8 file using AES decryption
// //           const decryptionKey = 'your-decryption-key'; // Replace with your decryption key
// //           const decipher = crypto.createDecipher('aes-256-cbc', decryptionKey);
// //           let decryptedM3U8 = decipher.update(encryptedM3U8, 'hex', 'utf8');
// //           decryptedM3U8 += decipher.final('utf8');

// //           // Load the decrypted M3U8 file into the HLS.js instance
// //           hls.loadSource(decryptedM3U8);
// //           hls.attachMedia(videoElement);
// //           hls.on(Hls.Events.MANIFEST_PARSED, () => {
// //             // Wait for user interaction before playing
// //             videoElement.addEventListener('play', handlePlay, { once: true });
// //           });
// //         } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
// //           videoElement.src = 'http://localhost:3100/videos/playlist';
// //           // Wait for user interaction before playing
// //           videoElement.addEventListener('play', handlePlay, { once: true });
// //         }
// //       })();
// //     }

// //     return () => {
// //       if (hls) {
// //         hls.destroy();
// //       }
// //     };
// //   }, []);

// //   return (
// //     <div>
// //       <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
// //     </div>
// //   );
// // };

// // export default VideoPlayerDash;


// import React, { useEffect, useRef } from 'react';
// import Hls from 'hls.js';

// const VideoPlayerDash = () => {
//   const videoRef = useRef(null);
//   let hls;

//   const handlePlay = () => {
//     if (videoRef.current && videoRef.current.paused) {
//       videoRef.current.play();
//     }
//   };

//   useEffect(() => {
//     if (videoRef.current) {
//       const videoElement = videoRef.current;

//       if (Hls.isSupported()) {
//         hls = new Hls();
//         hls.loadSource('http://localhost:3000/hls/your-m3u8-filename.m3u8');
//         hls.attachMedia(videoElement);

//         hls.on(Hls.Events.MEDIA_ATTACHED, () => {
//           // Wait for media to be attached before loading the manifest
//           hls.loadSource('http://localhost:3000/hls/your-m3u8-filename.m3u8');
//         });

//         hls.on(Hls.Events.MANIFEST_PARSED, () => {
//           // Wait for user interaction before playing
//           videoElement.addEventListener('play', handlePlay, { once: true });
//         });
//       } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
//         videoElement.src = 'http://localhost:3000/hls/your-m3u8-filename.m3u8';
//         // Wait for user interaction before playing
//         videoElement.addEventListener('play', handlePlay, { once: true });
//       }
//     }

//     return () => {
//       if (hls) {
//         hls.destroy();
//       }
//     };
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
//     </div>
//   );
// };

// export default VideoPlayerDash;
