import React, { useEffect, useRef } from 'react';
import Hls from 'hls.js';

const VideoPlayerDash = () => {
  const videoRef = useRef(null);
  let hls;

  const handlePlay = () => {
    if (videoRef.current && videoRef.current.paused) {
      videoRef.current.play();
    }
  };

  useEffect(async () => {
    if (videoRef.current) {
      const videoElement = videoRef.current;
      

      if (Hls.isSupported()) {
        hls = new Hls();
        hls.loadSource('http://localhost:3000/hls/output.m3u8');
        hls.attachMedia(videoElement);
        hls.on(Hls.Events.MANIFEST_PARSED, () => {
          // Wait for user interaction before playing
          videoElement.addEventListener('play', handlePlay, { once: true });
        });
      } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
        videoElement.src = 'http://localhost:3000/hls/output.m3u8';
        // Wait for user interaction before playing
        videoElement.addEventListener('play', handlePlay, { once: true });
      }
    }

    return () => {
      if (hls) {
        hls.destroy();
      }
    };
  }, []);

  return (
    <div>
      <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
    </div>
  );
};

export default VideoPlayerDash;
