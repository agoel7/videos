// import React, { useEffect, useRef } from 'react';
// import videojs from 'video.js';

// const HLSVideoPlayer = ({ source }) => {
//   const videoRef = useRef(null);

//   useEffect(() => {
//     const videoElement = videoRef.current;

//     // Initialize the Video.js player
//     const player = videojs(videoElement, {
//       html5: {
//         hls: {
//           overrideNative: true, // Enable HLS playback in Safari
//         },
//       },
//       plugins: {
//         contribHls: {},
//       },
//     });

//     // Set the HLS source
//     player.src({
//       src: source,
//       type: 'application/x-mpegURL',
//     });

//     // Dispose the player when the component unmounts
//     return () => {
//       player.dispose();
//     };
//   }, [source]);

//   return (
//     <div data-vjs-player>
//       <video ref={videoRef} controls></video>
//     </div>
//   );
// };

// export default HLSVideoPlayer;
