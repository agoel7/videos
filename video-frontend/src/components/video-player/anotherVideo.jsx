// // GIVES UNCAUGHT REFRENCE ERROR ARGUMENTS NOT FOUND 

// // import React, { useEffect, useRef } from 'react';

// // const VideoPlayer2 = () => {
// //   const videoRef = useRef(null);

// //   useEffect(() => {
// //     const videoElement = videoRef.current;
// //     if (videoElement) {
// //       fetch('http://localhost:3100/videos/1', {
// //         headers: {
// //           'Range': 'bytes=0-',
// //         },
// //       })
// //         .then((response) => {
// //           // Check if the response code is 206 (Partial Content)
// //           if (response.status === 206) {
// //             const reader = response.body.getReader();
// //             const contentLength = parseInt(response.headers.get('Content-Length') || '0', 10);
// //             let receivedLength = 0;
// //             let chunks = [];

// //             // Read the response stream in chunks
// //             function readChunk() {
// //               reader.read().then(({ done, value }) => {
// //                 if (done) {
// //                   // Combine the received chunks into a single Uint8Array
// //                   const videoData = new Uint8Array(receivedLength);
// //                   let offset = 0;
// //                   for (const chunk of chunks) {
// //                     videoData.set(chunk, offset);
// //                     offset += chunk.length;
// //                   }

// //                   // Create a Blob from the video data
// //                   const blob = new Blob([videoData], { type: 'video/mp4' });

// //                   // Create an object URL for the Blob
// //                   const videoUrl = URL.createObjectURL(blob);

// //                   // Set the object URL as the video source
// //                   videoElement.src = videoUrl;
// //                 } else {
// //                   receivedLength += value.length;
// //                   chunks.push(value);
// //                   readChunk();
// //                 }
// //               });
// //             }

// //             readChunk();
// //           }
// //         })
// //         .catch((error) => {
// //           console.error('Error fetching video:', error);
// //         });
// //     }
// //   }, []);

// //   return (
// //     <div>
// //       <video ref={videoRef} controls>
// //         {/* Add fallback content here */}
// //       </video>
// //     </div>
// //   );
// // };

// // export default VideoPlayer2;

// // APPROACH 2
// // import React, { useEffect, useRef } from 'react';

// // const VideoPlayer2 = () => {
// //   const videoRef = useRef(null);

// //   useEffect(() => {
// //     const videoUrl = 'http://localhost:3100/videos/1'; // Replace with the URL of your video hosted on Digital Ocean

// //     // Fetch the video URL or access token from your backend to ensure secure access

// //     videoRef.current.src = videoUrl;
// //     videoRef.current.load();

// //     // Cleanup
// //     return () => {
// //       // Clean up any resources if needed
// //       // E.g., revoke URL.createObjectURL or close MediaSource
// //     };
// //   }, []);

// //   return (
// //     <div>
// //     {console.log(videoRef)}
// //       <video autoPlay={true} ref={videoRef} controls />
// //     </div>
// //   );
// // };

// // export default VideoPlayer2;
// import React, { useEffect, useRef } from 'react';
// import videojs from 'video.js';
// import 'video.js/dist/video-js.css';

// const VideoPlayer2 = ({ videoBlob }) => {
//   const videoRef = useRef(null);

//   useEffect(() => {
//     // Create a Blob URL for the Blob object
//     const videoBlobUrl = URL.createObjectURL(videoBlob);

//     // Initialize Video.js on the HTML5 video element
//     const player = videojs(videoRef.current, {
//       sources: [{
//         src: videoBlobUrl,
//         type: 'video/mp4'
//       }]
//     });

//     return () => {
//       // Dispose of the Video.js player when the component unmounts
//       if (player) {
//         player.dispose();
//       }
//     };
//   }, [videoBlob]);

//   return (
//     <div>
//       <video ref={videoRef} className="video-js" controls />
//     </div>
//   );
// };

// export default VideoPlayer2;
