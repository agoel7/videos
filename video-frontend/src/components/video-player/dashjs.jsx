// import React, { useEffect, useRef } from 'react';
// import Hls from 'hls.js';
// import axios from 'axios';

// const VideoPlayer = () => {
//   const videoRef = useRef(null);
//   let hls;

//   const handlePlay = () => {
//     if (videoRef.current && videoRef.current.paused) {
//       videoRef.current.play();
//     }
//   };

//   const getPlaylistUrl = async () => {
//     const response = await axios.get('http://localhost:3100/proxy/masterclass/getPlaylist');
//     return response.data;
//   };

//   useEffect(() => {
//     if (videoRef.current) {
//       const videoElement = videoRef.current;
//       (async () => {
//         const playlistUrl = await getPlaylistUrl();
//         if (!playlistUrl) {
//           throw new Error('Playlist URL Not Found');
//         }
//         if (Hls.isSupported()) {
//           hls = new Hls();
//           hls.loadSource(playlistUrl);
//           hls.attachMedia(videoElement);
//           hls.on(Hls.Events.MANIFEST_PARSED, () => {
//             // Wait for user interaction before playing
//             videoElement.addEventListener('play', handlePlay, { once: true });
//           });
//         } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
//           videoElement.src = playlistUrl;
//           // Wait for user interaction before playing
//           videoElement.addEventListener('play', handlePlay, { once: true });
//         }
//       })();
//     }

//     return () => {
//       if (hls) {
//         hls.destroy();
//       }
//     };
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
//     </div>
//   );
// };

// export default VideoPlayer;


// // import React, { useEffect, useRef, useState } from 'react';
// // import Hls from 'hls.js';
// // import axios from 'axios';

// // const VideoPlayerDash = () => {
// //   const videoRef = useRef(null);
// //   const [playlistUrl, setPlaylistUrl] = useState('');

// //   const handlePlay = () => {
// //     if (videoRef.current && videoRef.current.paused) {
// //       videoRef.current.play();
// //     }
// //   };

// //   useEffect(() => {
// //     const fetchPlaylist = async () => {
// //       try {
// //         const response = await axios.get('http://localhost:3100/proxy/masterclass/getPlaylist');
// //         const playlistData = response.data;

// //         setPlaylistUrl(playlistData);
// //       } catch (error) {
// //         console.error('Error occurred:', error);
// //       }
// //     };

// //     fetchPlaylist();
// //   }, []);

// //   useEffect(() => {
// //     if (playlistUrl) {
// //       if (Hls.isSupported()) {
// //         const hls = new Hls();
// //         hls.loadSource(playlistUrl);
// //         hls.attachMedia(videoRef.current);
// //         hls.on(Hls.Events.MANIFEST_PARSED, () => {
// //           // Wait for user interaction before playing
// //           videoRef.current.addEventListener('play', handlePlay, { once: true });
// //         });
// //       } else if (videoRef.current.canPlayType('application/vnd.apple.mpegurl')) {
// //         videoRef.current.src = playlistUrl;
// //         // Wait for user interaction before playing
// //         videoRef.current.addEventListener('play', handlePlay, { once: true });
// //       }
// //     }
// //   }, [playlistUrl]);

// //   return (
// //     <div>
// //       <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
// //     </div>
// //   );
// // };

// // export default VideoPlayerDash;



// // WITH AUTHENTICATION USING JWT

// import React, { useEffect, useRef } from 'react';
// import Hls from 'hls.js';
// import axios from 'axios';

// const VideoPlayerDash = () => {
//   const videoRef = useRef(null);
//   let hls;

//   const handlePlay = () => {
//     if (videoRef.current && videoRef.current.paused) {
//       videoRef.current.play();
//     }
//   };

//   const getToken =async () => {
//   const response = await axios.get('http://localhost:3100/proxy/masterclass/getAuthToken')
//     return response.data
//   }
//   const tokenAuthentication = async (token) => {
//     const response = await axios.get('http://localhost:3100/proxy/masterclass/authenticate', {
//       headers: {
//         Authorization: `Bearer ${token}`
//       }
//     })
//     return response.data
//   }



//   useEffect(async () => {
//     if (videoRef.current) {
//       const videoElement = videoRef.current;
//       const token = await getToken()
//       console.log(token);
//       if(!token){
//         throw new Error('Unauthorized access')
//       }
//       const verifyToken = tokenAuthentication(token)

//       if (verifyToken && Hls.isSupported()) {
//         hls = new Hls();
//         // const loadSource = loadSource()
//         hls.loadSource(' ');
//         hls.attachMedia(videoElement);
//         hls.on(Hls.Events.MANIFEST_PARSED, () => {
//           // Wait for user interaction before playing
//           videoElement.addEventListener('play', handlePlay, { once: true });
//         });
//       } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
//         videoElement.src = 'https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8';
//         // Wait for user interaction before playing
//         videoElement.addEventListener('play', handlePlay, { once: true });
//       }
//     }

//     // return () => {
//     //   if (hls && typeof hls.destroy === 'function') {
//     //     hls.destroy();
//     //   }
      
//     // };
//     return () => {
//             if (hls) {
//               hls.destroy();
//             }
//           };
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
//     </div>
//   );
// };

// export default VideoPlayerDash;



// WITHOUT AUTHENTICATION TRYING TO SEND HLS INSTANCE ON FRONTEND

// import React, { useEffect, useRef } from 'react';
// import Hls from 'hls.js';
// import axios from 'axios';

// const VideoPlayerDash = () => {
//   const videoRef = useRef(null);
//   let hls;

//   const handlePlay = () => {
//     if (videoRef.current && videoRef.current.paused) {
//       videoRef.current.play();
//     }
//   };

//   useEffect(() => {
//     if (videoRef.current) {
//       const videoElement = videoRef.current;
//       let token;
//       (async () => {
//         if (Hls.isSupported()) {
//           hls = new Hls();
//           // const formData = new FormData();
//           // formData.append('hlsInstance',hls)
//           // const videoElement = await axios.post('http://localhost:3100/proxy/masterclass/loadSource',formData)
//           hls.loadSource('https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8');
//           hls.attachMedia(videoElement);
//           hls.on(Hls.Events.MANIFEST_PARSED, () => {
//             // Wait for user interaction before playing
//             videoElement.addEventListener('play', handlePlay, { once: true });
//           });
//         } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
//           videoElement.src = 'https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8';
//           // Wait for user interaction before playing
//           videoElement.addEventListener('play', handlePlay, { once: true });
//         }
//       })()
//     }

//     return () => {
//       if (hls) {
//         hls.destroy();
//       }
//     };
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
//     </div>
//   );
// };

// export default VideoPlayerDash;



import React, { useEffect, useRef } from 'react';
import Hls from 'hls.js';
import axios from 'axios';

const VideoPlayerDash = () => {
  const videoRef = useRef(null);
  let hls;

  const handlePlay = () => {
    if (videoRef.current && videoRef.current.paused) {
      videoRef.current.play();
    }
  };

 const handleClick = () => {
  if (videoRef.current) {
        const videoElement = videoRef.current;
        let token;
        (async () => {
          if (Hls.isSupported()) {
            hls = new Hls();
            // hls.loadSource('/home/deq/hlsServer/express-hls-example/src/videos/output.m3u8');

            hls.loadSource('https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8');
            hls.attachMedia(videoElement);
            hls.on(Hls.Events.MANIFEST_PARSED, () => {
              // Wait for user interaction before playing
              videoElement.addEventListener('play', handlePlay, { once: true });
            });
          } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
            videoElement.src = 'https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8';
            // Wait for user interaction before playing
            videoElement.addEventListener('play', handlePlay, { once: true });
          }
        })()
      }
 }


  // useEffect(() => {
  //   if (videoRef.current) {
  //     const videoElement = videoRef.current;
  //     let token;
  //     (async () => {
  //       if (Hls.isSupported()) {
  //         hls = new Hls();
  //         hls.loadSource('https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8');
  //         hls.attachMedia(videoElement);
  //         hls.on(Hls.Events.MANIFEST_PARSED, () => {
  //           // Wait for user interaction before playing
  //           videoElement.addEventListener('play', handlePlay, { once: true });
  //         });
  //       } else if (videoElement.canPlayType('application/vnd.apple.mpegurl')) {
  //         videoElement.src = 'https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8';
  //         // Wait for user interaction before playing
  //         videoElement.addEventListener('play', handlePlay, { once: true });
  //       }
  //     })()
  //   }

  //   return () => {
  //     if (hls) {
  //       hls.destroy();
  //     }
  //   };
  // }, []);

  return (
    <div>
    <button onClick={handleClick}>Play</button>
      <video preload='none' ref={videoRef} controls style={{ width: '100%', height: 'auto' }} />
    </div>
  );
}


export default VideoPlayerDash;





// WITHOUT CHECKING BROWSER SUPPORT (SHAKA PLAYER)

// import React, { useEffect, useRef } from 'react';
// import shaka from 'shaka-player';

// const VideoPlayerDash = () => {
//   const videoRef = useRef(null);

//   useEffect(() => {
//     if (videoRef.current) {
//       shaka.polyfill.installAll(); // Install necessary polyfills
//       const videoElement = videoRef.current;
//       const player = new shaka.Player(videoElement);
//       player.load('https://honeyland-images.sfo3.digitaloceanspaces.com/HLS-Playlist/output.m3u8').catch((error) => {
//         console.error('Error loading manifest:', error);
//       });
//     }
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls autoPlay style={{ width: '100%', height: 'auto' }} />
//     </div>
//   );
// };

// export default VideoPlayerDash;

// CHECKING BROWSER SUPPORT (SHAKA PLAYER)
// import React, { useEffect, useRef } from 'react';
// import shaka from 'shaka-player';

// const VideoPlayerDash = () => {
//   const videoRef = useRef(null);

//   useEffect(() => {
//     if (videoRef.current) {
//       if (shaka.Player.isBrowserSupported()) {
//         shaka.polyfill.installAll(); // Install necessary polyfills
//         const videoElement = videoRef.current;
//         const player = new shaka.Player(videoElement);

//         player.load('https://honeyland-images.sfo3.digitaloceanspaces.com/manifest-files/output_720p.mpd').catch((error) => {
//           console.error('Error loading manifest:', error);
//         });
//       } else {
//         console.error('Browser not supported');
//       }
//     }
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls autoPlay style={{ width: '100%', height: 'auto' }} />
//     </div>
//   );
// };

// export default VideoPlayerDash;


// USING DASHJS (NOT SUPPORTED ON IOS AND MAC)
// import React, { useEffect, useRef } from 'react';
// import dashjs from 'dashjs';

// const VideoPlayerDash = () => {
//   const videoPlayerRef = useRef(null);

//   useEffect(() => {
//     const manifestUrl = 'https://honeyland-images.sfo3.digitaloceanspaces.com/manifest-files/output_720p.mpd'; // Replace with your DASH manifest URL

//     const videoPlayer = dashjs.MediaPlayer().create();
//     videoPlayer.initialize(videoPlayerRef.current, manifestUrl);

//     return () => {
//       videoPlayer.reset();
//     };
//   }, []);

//   return (
//     <div>
//       <video preload='none' autoPlay='false' ref={videoPlayerRef} controls>
//         {/* No need to provide the source here */}
//       </video>
//     </div>
//   );
// };

// export default VideoPlayerDash;


// VideoJS Code:
// import React, { useEffect, useRef } from 'react';
// import videojs from 'video.js';

// const VideoPlayerDash = () => {
//   const videoPlayerRef = useRef(null);

//   useEffect(() => {
//     const manifestUrl = 'https://honeyland-images.sfo3.digitaloceanspaces.com/manifest-files/output_720p.mpd'; // Replace with your DASH manifest URL

//     // Initialize Video.js player
//     const player = videojs(videoPlayerRef.current);

//     // Load the MPEG-DASH manifest
//     player.src({
//       src: manifestUrl,
//       type: 'application/dash+xml',
//     });

//     // Dispose the player when the component is unmounted
//     return () => {
//       if (player) {
//         player.dispose();
//       }
//     };
//   }, []);

//   return (
//     <div>
//       <video ref={videoPlayerRef} controls>
//         {/* No need to provide the source here */}
//       </video>
//     </div>
//   );
// };
// export default VideoPlayerDash;
