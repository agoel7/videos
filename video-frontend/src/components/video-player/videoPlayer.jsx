

// Code using the blob (but downloading whole video first)

// import React, { useEffect, useState } from 'react';

// const VideoPlayer = () => {
//   const [videoUrl, setVideoUrl] = useState('');
//   const videoEndpoint = 'http://localhost:3100/videos/1'; // Replace with your backend endpoint

//   useEffect(() => {
//     const fetchVideo = async () => {
//       try {
//         const response = await fetch(videoEndpoint);
//         if (!response.ok) {
//           throw new Error('Network response was not OK');
//         }
//         const videoBlob = await response.blob();
//         const videoURL = URL.createObjectURL(videoBlob);
//         console.log("videoURL",videoURL);
//         setVideoUrl(videoURL);
//       } catch (error) {
//         console.error('Error fetching the video:', error);
//       }
//     };

//     fetchVideo();
//   }, []);

//   const handleContext = (e) => {
//     e.preventDefault();
//   };

//   return (
//     <div>
//     <h1>Video</h1>
//         <video controls autoPlay onContextMenu={handleContext}>
//           <source src={videoUrl} type="video/mp4" />
//           Your browser does not support the video tag.
//         </video>
//     </div>
//   );
// };

// export default VideoPlayer;

// import React, { useEffect, useRef, useState } from 'react';
// import videojs from 'video.js';
// import 'video.js/dist/video-js.css';

// const VideoPlayer = () => {
//   const videoRef = useRef(null);
//   const playerRef = useRef(null);
//   const videoEndpoint = 'http://localhost:3100/videos/1'; // Replace with your backend endpoint
//   const [isLoaded, setIsLoaded] = useState(false);

//   useEffect(() => {
//     const initializePlayer = () => {
//       const videoElement = videoRef.current;
//       const playerInstance = videojs(videoElement);

//       // Set the video source
//       playerInstance.src({ src: videoEndpoint, type: 'video/mp4' });

//       // Load the video
//       playerInstance.load();

//       // Store the player instance in the ref
//       playerRef.current = playerInstance;

//       // Listen for the 'loadeddata' event to know when the first chunk is loaded
//       playerInstance.on('loadeddata', handleLoadedData);
//     };

//     initializePlayer();

//     return () => {
//       if (playerRef.current) {
//         playerRef.current.dispose();
//       }
//     };
//   }, []);

//   const handleLoadedData = () => {
//     setIsLoaded(true);
//     playerRef.current.play();
//   };

//   return (
//     <div>
//       <div className="play-button" onClick={() => playerRef.current.play()}>Play</div>
//       {isLoaded && (
//         <video ref={videoRef} className="video-js vjs-default-skin" controls />
//       )}
//     </div>
//   );
// };

// export default VideoPlayer;

// CHALU CODE!!!!

// import React, { useEffect, useRef, useState } from 'react';
// import axios from 'axios';

// const VideoPlayer = () => {
//   const videoRef = useRef(null);
//   const [blobUrl, setBlobUrl] = useState('');

//   useEffect(() => {
//     const fetchVideo = async () => {
//       const response = await axios.get('http://localhost:3100/videos/1', {
//         responseType: 'blob',
//       });

//       const videoBlob = new Blob([response.data], { type: 'video/mp4' });
//       const url = URL.createObjectURL(videoBlob);
//       setBlobUrl(url);
//     };

//     fetchVideo();

//     return () => {
//       if (blobUrl) {
//         URL.revokeObjectURL(blobUrl);
//       }
//     };
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} src={blobUrl} width='500px' height='500px' controls />
//     </div>
//   );
// };

// export default VideoPlayer;


// MSE AND FETCH 
// import React, { useEffect, useRef } from 'react';

// const VideoPlayer = () => {
//   const videoRef = useRef(null);
//   const mediaSourceRef = useRef(null);
//   const sourceBufferRef = useRef(null);
//   const videoEndpoint = 'http://localhost:3100/videos/1'; // Replace with your backend endpoint

//   useEffect(() => {
//     const initializePlayer = async () => {
//       const mediaSource = new MediaSource();
//       mediaSourceRef.current = mediaSource;

//       const videoElement = videoRef.current;
//       videoElement.src = URL.createObjectURL(mediaSource);

//       mediaSource.addEventListener('sourceopen', handleSourceOpen);
//     };

//     initializePlayer();

//     return () => {
//       if (mediaSourceRef.current) {
//         mediaSourceRef.current.removeEventListener('sourceopen', handleSourceOpen);
//         mediaSourceRef.current = null;
//       }
//     };
//   }, []);

//   const handleSourceOpen = () => {
//     const mediaSource = mediaSourceRef.current;
//     const sourceBuffer = mediaSource.addSourceBuffer('video/mp4');
//     sourceBufferRef.current = sourceBuffer;

//     fetchVideoStream();
//   };

//   const fetchVideoStream = async () => {
//     const response = await fetch(videoEndpoint);
//     const reader = response.body.getReader();

//     const pump = async () => {
//       const { done, value } = await reader.read();

//       if (done) {
//         sourceBufferRef.current.removeEventListener('updateend', pump);
//         mediaSourceRef.current.endOfStream();
//         return;
//       }

//       sourceBufferRef.current.addEventListener('updateend', pump);
//       sourceBufferRef.current.appendBuffer(value);

//       const blob = new Blob([value], { type: 'video/mp4' });
//       const blobUrl = URL.createObjectURL(blob);
//       videoRef.current.src = blobUrl;

//       // Play the video in response to a user interaction
//       videoRef.current.addEventListener('loadeddata', () => {
//         videoRef.current.play();
//       }, { once: true });
//     };

//     pump();
//   };

//   return (
//     <div>
//       <video ref={videoRef} width='100vw' height='100vh' controls />
//     </div>
//   );
// };

// export default VideoPlayer;

// Apending in blob object
// Fetch video data in chunks or streams
// import React, { useEffect, useRef } from 'react';

// const VideoPlayer = () => {
//   const videoRef = useRef(null);
//   const streamEndpoint = 'http://localhost:3000/videos/1'; // Replace with your backend stream endpoint

//   useEffect(() => {
//     const fetchVideoChunks = async () => {
//       const response = await fetch(streamEndpoint);
//       const reader = response.body.getReader();
//       const mediaSource = new MediaSource();
//       let sourceBuffer;

//       videoRef.current.src = URL.createObjectURL(mediaSource);

//       mediaSource.addEventListener('sourceopen', async () => {
//         sourceBuffer = mediaSource.addSourceBuffer('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
//         sourceBuffer.mode = 'sequence';

//         const processVideoChunks = async () => {
//           while (true) {
//             const { done, value } = await reader.read();

//             if (done) {
//               if (!sourceBuffer.updating) {
//                 mediaSource.endOfStream();
//                 break;
//               } else {
//                 // Wait for the source buffer to finish updating before calling endOfStream
//                 await new Promise((resolve) => {
//                   sourceBuffer.addEventListener('updateend', resolve);
//                 });
//                 mediaSource.endOfStream();
//                 break;
//               }
//             }

//             sourceBuffer.appendBuffer(value);
//           }
//         };

//         processVideoChunks();
//       });

//       mediaSource.addEventListener('error', (error) => {
//         console.error('Error in MediaSource:', error);
//       });
//     };

//     fetchVideoChunks();
//   }, []);

//   return (
//     <div>
//       <video ref={videoRef} controls />
//     </div>
//   );
// };

// export default VideoPlayer;
