import { Body, Controller, Get, Param, Post, Query, Req, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { Request, Response } from 'express';
import  HlsServer from 'hls-server'
import { exec } from 'child_process';
import axios, { AxiosResponse } from 'axios';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken'
import { FileInterceptor } from '@nestjs/platform-express';
import crypto from 'crypto';



@Controller('proxy/masterclass')
export class VideoController {

  @Get('getAuthToken')
  async getAuthToken(@Res() res: Response) {    
    const payload = {name: 'Steady-Stack', sub:'AuthToken'}
    const token = jwt.sign(payload,'SecretKeyForJWTVideoAuthForSteadyStack',{expiresIn:1000 * 60})
    res.send(token)
  }

  @Get('authenticate')
  async authenticate(@Req() req: Request,@Res() res: Response) {
    const token = req.headers.authorization.split(' ')[1]
    jwt.verify(token,'SecretKeyForJWTVideoAuthForSteadyStack',(err) => {
      if(err){
        res.send(false)
      }
    })
    res.send(true)
    
  }
}

//   @Get('manifest')
//   async getManifest(@Res() res: Response): Promise<void>{
//     const manifestContent = await axios.get('https://honeyland-images.sfo3.digitaloceanspaces.com/manifest-files/output_720p.mpd')
//     const initFileName = manifestContent.data
//     // const chunkContent = await responseInitFile.data;
//     res.send(initFileName);
//   }













//   @Get('generateManifest/:id')
//   async generateManifest(@Param('id') id: string, @Res() res: Response): Promise<void> {
//     try {
//       const videoUrl = 'https://byt.nyc3.cdn.digitaloceanspaces.com/media/steadystack/week1.mp4'; // Replace with your DigitalOcean Spaces video URL
//       const videoFilePath = `/home/deq/VideoWork/DemoVideo/video-work/src/manifests/${id}.mp4` // Replace with the desired local video file path
//       const outputFilePath = `/home/deq/VideoWork/DemoVideo/video-work/src/manifests/${id}.mpd` // Replace with the desired output manifest file path

//       // Download the video file locally
//       const response: AxiosResponse<any, any> = await axios.get(videoUrl, { responseType: 'stream' });

//       const writer = fs.createWriteStream(videoFilePath);
//       response.data.pipe(writer);

//       await new Promise((resolve, reject) => {
//         writer.on('finish', resolve);
//         writer.on('error', reject);
//       });

//       // Generate DASH manifest using MP4Box
//       const command = `MP4Box -dash 10000 -frag 1000 -rap -profile dashavc264:onDemand -out ${outputFilePath} ${videoFilePath}`;
//       exec(command, (error, stdout, stderr) => {
//         if (error) {
//           console.error('Error generating DASH manifest:', error);
//           res.status(500).send('Error generating DASH manifest');
//         } else {
//           console.log('DASH manifest generated successfully');

//           // Read the manifest file
//           const manifestContent = fs.readFileSync(outputFilePath, 'utf-8');

//           // Set appropriate headers for CDN caching
//           res.setHeader('Content-Type', 'application/dash+xml');
//           res.setHeader('Cache-Control', 'public, max-age=31536000'); // Adjust cache control as per your requirements

//           // Send the manifest file to the frontend
//           // res.send(manifestContent);
//         }

//         // Clean up: Delete the downloaded video file

//         fs.unlink(videoFilePath, (unlinkError) => {
//           if (unlinkError) {
//             console.error('Error deleting video file:', unlinkError);
//           }
//         });
//       });
//     } catch (error) {
//       console.error('Error generating DASH manifest:', error);
//       res.status(500).send('Error generating DASH manifest');
//     }
//   }
// }






// // @Controller('videos')
// // export class VideoController {





// //   // Handling the streaming on backend
// //   @Get(':id')
// //   async streamVideo(@Param('id') id: string, @Req() req: Request, @Res() res: Response): Promise<void> {
// //     try {
// //       const videoUrl = 'https://byt.nyc3.cdn.digitaloceanspaces.com/media/steadystack/week1.mp4';
// //       const response = await axios.head(videoUrl);
// //       const fileSize = parseInt(response.headers['content-length'], 10);
// //       const range = req.headers.range;

// //       if (range) {
// //         const parts = range.replace(/bytes=/, '').split('-');
// //         const start = parseInt(parts[0], 10);
// //         const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
// //         const chunkSize = end - start + 1;

// //         res.writeHead(206, {
// //           'Content-Range': `bytes ${start}-${end}/${fileSize}`,
// //           'Accept-Ranges': 'bytes',
// //           'Content-Length': chunkSize,
// //           'Content-Type': 'video/mp4',
// //         });

// //         const videoStream = await axios.get(videoUrl, {
// //           headers: {
// //             Range: req.headers.range,
// //           },
// //           responseType: 'stream',
// //         });

// //         videoStream.data.pipe(res);
// //       } else {
// //         res.writeHead(200, {
// //           'Content-Length': fileSize,
// //           'Content-Type': 'video/mp4',
// //         });

// //         const videoStream = await axios.get(videoUrl, { responseType: 'stream' });
// //         videoStream.data.pipe(res);
// //       }
// //     } catch (error) {
// //       console.error('Error streaming video:', error);
// //       res.status(500).send('Error streaming video');
// //     }
// //   }


// //     // Handling the streaming using HLS
// //     @Get('hls/:id')
// //     hlsVideo(){
// //       const videoPath = '/home/deq/Downloads/p.mp4'; // Replace with the URL of your video hosted on DigitalOcean
// //       const outputDir = 'DemoVideo/video-work/src/hls' ; // Output directory for the HLS chunks
// //       console.log(outputDir);
      
  
  
// //       // Run FFmpeg command to generate HLS chunks
// //       ffmpeg(videoPath)
// //         .outputOptions('-hls_time', '10') // Set the duration of each HLS chunk (in seconds)
// //         .outputOptions('-hls_list_size', '1') // Set the HLS playlist size (0 for unlimited)
// //         .outputOptions('-hls_segment_filename', `${outputDir}/chunk%03d.ts`) // Set the filename pattern for HLS chunks
// //         .output(`${outputDir}/playlist.m3u8`) // Output HLS playlist
// //         .on('end', () => {
// //           console.log('HLS chunks generated successfully.');
// //         })
// //         .on('error', (err) => {
// //           console.error('Error generating HLS chunks:', err);
// //         })
// //         .run();
  
// //       return 'HLS chunks generation initiated.';
// //     }
  

// // }
